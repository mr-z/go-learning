# go-learning

#### 介绍
日常学习

#### 软件架构
基于Golang 的 Iris web 框架

### 依赖下载
go get github.com/kataras/iris   web框架


go get github.com/go-sql-driver/mysql    mysql驱动下载

#### 安装教程
**使用 Golang 的 Iris web 框架时,用 go get github.com/kataras/iris 命令久久无法下载

1.打开CMD命令提示框，设置临时环境变量
set GO111MODULE=on

set GOPROXY=https://goproxy.io

**注意 https://goproxy.io 也可替换成阿里云的 https://mirrors.aliyun.com/goproxy

2.再次执行 go get github.com/kataras/iris 也可以加 -v -u ，如下（执行时，切换到GOPATH的目录执行该命令）

 go get -v -u github.com/kataras/iris
值得注意的是，这些包并没有下载到 GOPATH/src 目录下，而是在 GOPATH/pkg/mod 目录中，需要手动copy到GOPATH/src目录下
#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技
