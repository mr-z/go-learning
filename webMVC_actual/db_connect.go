package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"reflect"
	"strings"
)

//数据库配置
const (
	userName = "root"
	pwd      = "123456"
	ip       = "192.168.2.51"
	port     = "3307"
	dbName   = "go-test"
)

//Db数据库连接池
var DB *sql.DB

//方法名大写，就是public
func InitDB() {
	//构建连接："用户名:密码@tcp(IP:端口)/数据库?charset=utf8"
	path := strings.Join([]string{userName, ":", pwd, "@tcp(", ip, ":", port, ")/", dbName, "?charset=utf8"}, "")
	DB, _ = sql.Open("mysql", path)
	//设置最大连接数
	DB.SetConnMaxLifetime(100)
	//设置上数据库最大闲置连接数
	DB.SetMaxIdleConns(10)
	if err := DB.Ping(); err != nil {
		fmt.Println("open database failed")
		return
	}
	fmt.Println("connect success")
}

func InsertUser(user User) bool {
	//开启事物
	tx, err := DB.Begin()
	if err != nil {
		fmt.Println("tx failed")
		return false
	}
	//准备sql语句
	stmt, err := tx.Prepare("INSERT INTO user (`name`, `pwd` , `addr`) VALUES (?, ?, ?)")
	if err != nil {
		fmt.Println("Prepare fail")
		return false
	}
	res, err := stmt.Exec("张三", "123456", "虹梅南路3833号")
	if err != nil {
		fmt.Println("exec fail")
		return false
	}
	//事物提交
	tx.Commit()
	//获得自增id
	fmt.Println(res.LastInsertId())
	return true
}

//user实体
type User struct {
	name string
	pwd  string
	addr string
}

func main() {
	user := User{name: "zhangsan", pwd: "123", addr: "111"}
	InitDB()
	InsertUser(user)
	//构建连接, 格式是：”用户名:密码@tcp(IP:端口)/数据库?charset=utf8”
	db, err := sql.Open("mysql", "root:Jp123@@tcp(192.168.2.51:3307)/go-test?charset=utf8")
	checkErr(err)
	//db.Query("drop database if exists tmpdb")
	//db.Query("create database tmpdb")
	////db.Query("use tmpdb")
	//db.Query("create table tmpdb.tmptab(id int, name varchar(255), addr varchar(255))")
	db.Query("insert into tmpdb.tmptab values(101, '姓名1', 'address1'), (102, '姓名2', 'address2'), (103, '姓名3', 'address3'), (104, '姓名4', 'address4')")
	query, err := db.Query("select * from tmpdb.tmptab")
	checkErr(err)
	v := reflect.ValueOf(query)
	fmt.Println(v)
	fmt.Println("--增加数据测试--")
	printResult(query)
	db.Close()
}

func printResult(query *sql.Rows) {
	column, _ := query.Columns()              //读出查询出的列字段名
	values := make([][]byte, len(column))     //values是每个列的值，这里获取到byte里
	scans := make([]interface{}, len(column)) //因为每次查询出来的列是不定长的，用len(column)定住当次查询的长度
	for i := range values {                   //让每一行数据都填充到[][]byte里面
		scans[i] = &values[i]
	}
	results := make(map[int]map[string]string) //最后得到的map
	i := 0
	for query.Next() { //循环，让游标往下移动
		//query.Scan查询出来的不定长值放到scans[i] = &values[i],也就是每行都放在values里
		if err := query.Scan(scans...); err != nil {
			fmt.Println(err)
			return
		}
		row := make(map[string]string) // 每行数据
		for k, v := range values {     //每行数据是放在values里面，现在把它挪到row里
			key := column[k]
			row[key] = string(v)
		}
		results[i] = row //装入结果集
		i++
	}
	for k, v := range results { //查询出来的数组
		fmt.Println(k, v)
	}
}

func checkErr(e error) {
	if e != nil {
		panic(e)
	}
}
