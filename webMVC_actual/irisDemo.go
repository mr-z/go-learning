package main

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"
)

func main() {
	app := iris.New()
	app.Logger().SetLevel("DEBUG")
	app.Use(recover.New())
	app.Use(logger.New())
	app.Get("/", func(ctx iris.Context) {
		ctx.HTML("<h3>Hello world! go!</h3>")
	})
	app.Run(iris.Addr(":8099"), iris.WithoutServerError(iris.ErrServerClosed))
}
